## Assembly instructions

#### Box

![Image 1: Box](/assets/images/A13b-01.jpg)

Lasercut the files for the box and assemble. Adjust the parameters in the file to account for the material thickness and laser cutter's kerf.

#### Scooper

![Image 2: Scooper](/assets/images/A13b-02.jpg)

Lasercut the scooper using the design file included in the repository. Using heat, bend the spikes 30 degrees to create a 3 dimensional rake.

![Image 3: Clips](/assets/images/A13b-03.jpg)

3D print 3 clips using the RodGrabber STL files included. Use 40% infill. Make sure to print the model on its side with the layers going from the flat side to the end of the clip.

![Image 4: Tape](/assets/images/A13b-04.jpg)

Take the 8mm smooth rod and check the rough placement of the scoop. Add some silicone tape or other flexible tape, such as self-binding repair tape. Slide in the first 3D printed clip before adding the next tape. Repeat twice more.

![Image 5: Attach scooper](/assets/images/A13b-05.jpg)

Attach the acrylic scooper to the rod clips by using six M4 nuts and bolts.

![Image 6: Tighten grip](/assets/images/A13b-06.jpg)

Tighten the grip on the 3D printed clips by using three M3 nuts and bolts.

#### Base

![Image 7: Base](/assets/images/base.JPG)

The base was made of wood with different thickness by laser cutting. The files are in the [DesignFiles folder](/DesignFiles/Base). We used:

- Top layer: base_1.ai --> 3mm MDF
- Second layer: base_2.ai --> 5mm plywood
- Third layer: base_2.ai --> 3mm MDF
- Bottom: base_3.ai --> This layer is mainly for adjusting the height of the machine. At the beginning, we used 6mm plywood but found it was too short, so we changed to 10mm wood pieces at the end.

#### Moving Motor

![Image 8: Moving Motor](/assets/images/motor1.JPG)

The moving structure was mainly driven by one stepper motor. The motor connects to one **lead screw** directly and also rotates another lead screw by **the belt**. To make the assamble process easlier, we made a stucture in the middle to adjust the tightness of the belt. All the files are in the [DesignFiles folder](/DesignFiles/Moving_motor).

#### Rotating Motor

![Image 9: Rotating Motor](/assets/images/rotating_part.jpg)

Another stepper motre was used to rotate the scoop. Considering that we need a stronger force to pull cat's poops together also hold the weight of scooper, we decided to use the stucture of a worm gear set. It makes the rotating speed much slower but creates more power.
