//must have some delay between STEP_PIN change
//need to stop when change the rotating direction
//haven't finished:emergency  stop, reset

#define DIR_PIN_1         5
#define STEP_PIN_1        2
#define DIR_PIN_2         7
#define STEP_PIN_2        4
#define ENABLE_PIN        8

char input; //get the command character from web page
int btn = 0; 
int start_flag = 0;
int scoop_flag = 0;


void setup() {
  Serial.begin(115200); //same as the server.py file
  pinMode(DIR_PIN_1,    OUTPUT);
  pinMode(STEP_PIN_1,   OUTPUT);
  pinMode(DIR_PIN_2,    OUTPUT);
  pinMode(STEP_PIN_2,   OUTPUT);
  pinMode(ENABLE_PIN, OUTPUT);
  digitalWrite(ENABLE_PIN, LOW);
}

void loop() {

  if (Serial.available()) {
    input = Serial.read(); // read the incoming data as string
    Serial.println(input); // check if Arduino get the command

    if (input == 'r') {//go right (motor1)
      btn = 1;
    }
    else if (input == 'l') {//go left (motor1)
      btn = 2;
    }
    else if (input == 'o') {//open scoop (motor2)
      btn = 3;
      input = "";
    }
    else if (input == 'c') {//close scoop (motor2)
      btn = 4;
    }
    else if (input == 'a') {//auto start
      btn = 5;
      input = "";
    }
    else if (input == 'v') {//auto reset
      btn = 6;
    }
    else { //stop both motors
      btn = 0;
    }

    Serial.println(btn);
  }

  if (btn == 1) {
    left();
  }
  else if (btn == 2) {
    right();
  }
  else if (btn == 3) {
    scoop_flag = 1;
    if (scoop_flag) {
      scoop_open(300);
    }
  }

  else if (btn == 4) {
    scoop_flag = 1;
    if (scoop_flag) {
      scoop_close(300);
    }
  }
  else if (btn == 5) {
    start_flag = 1;
    Serial.print("flag:");
    Serial.println(start_flag);
    if (start_flag) {
      auto_start(320000);
    }
    Serial.print("flag:");
    Serial.println(start_flag);
  }
  else if (btn == 6) {
    auto_reset();
  }
  else {
    stop_move();
  }
}



void left() { //keep rotating (motor1)
  digitalWrite(DIR_PIN_1, LOW);
  digitalWrite(STEP_PIN_1, LOW);
  delayMicroseconds(10);
  digitalWrite(STEP_PIN_1, HIGH);
  delayMicroseconds(100);
}

void right() { //keep rotating (motor1)
  digitalWrite(DIR_PIN_1, HIGH);
  digitalWrite(STEP_PIN_1, LOW);
  delayMicroseconds(10);
  digitalWrite(STEP_PIN_1, HIGH);
  delayMicroseconds(100);
}

void stop_move() { //stop both motors
  digitalWrite(STEP_PIN_1, LOW);
  digitalWrite(STEP_PIN_2, LOW);

}

//rotate to a special angle and stop (motor2)
void scoop_open(long steps) {
  digitalWrite(DIR_PIN_2, LOW);
  for (long i = 0; i < steps; i++) {
    digitalWrite(STEP_PIN_2, LOW);
    delayMicroseconds(400);
    digitalWrite(STEP_PIN_2, HIGH);
    delayMicroseconds(100);
  }
  stop_move();
  scoop_flag = 0;
  btn = 0;
}

void scoop_close(long steps) {
  digitalWrite(DIR_PIN_2, HIGH);
  for (long i = 0; i < steps; i++) {
    digitalWrite(STEP_PIN_2, LOW);
    delayMicroseconds(400);
    digitalWrite(STEP_PIN_2, HIGH);
    delayMicroseconds(100);
  }
  stop_move();
  scoop_flag = 0;
  btn = 0;
}


//auto cleaning mode
//go forward to push poos to one side
// -->rise up the scoop
// -->go backward to the start point
// -->put down the scoop
// steps is bigger, the scoop moves further

void auto_start(long steps) {
  digitalWrite(DIR_PIN_1, HIGH);
  for (long i = 0; i < steps; i++) {
    digitalWrite(STEP_PIN_1, LOW);
    delayMicroseconds(10);
    digitalWrite(STEP_PIN_1, HIGH);
    delayMicroseconds(100);
  }
  stop_move();
  delay(100);
  scoop_open(4000);
  delay(1000);
  digitalWrite(DIR_PIN_1, LOW);
  for (long i = 0; i < steps; i++) {
    digitalWrite(STEP_PIN_1, LOW);
    delayMicroseconds(10);
    digitalWrite(STEP_PIN_1, HIGH);
    delayMicroseconds(100);
  }
  stop_move();
  delay(100);
  scoop_close(4000);
  start_flag = 0;
  btn = 0;
}


void auto_reset() {

}
