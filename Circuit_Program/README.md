## About Circuit and Program

### Components

![Image 1: Main components](/assets/images/main_components.jpg)

We used **two stepper motors** as the main components to move the scoop and rotate the scoop. The main controller is **Arduino UNO**. We also used [**gShield**](https://www.adafruit.com/product/1750) to make the motor controlling easier.

### Circuit

![Image 2: Circuit](/assets/images/g-shield.jpeg)

- Combine g-shied and Arduino Uno.
- Connect two motors to the terminals of X Axis and Z Axis. Make sure the wires next to each other are connected inside the motor (ususally red & blue, green & black).
- Connect the V+ and GND from the voltage transformer, but make sure it isn't connected to the AC power.

### Power

![Image 3: Power](/assets/images/power.JPG)

The main power is **12V DC** from a voltage transformer which can convert AC to DC. We also need **5V DC** for Arduino. Because out control interface also need to connect to Arduino, we use a cable to connect the computer and Arduino, getting the power and the commands at the same time.

### Program

There are some commands in the contol_code file.

From the [schematic of g-shield](https://github.com/synthetos/grblShield/tree/master/hardware/gshield_v5_schematic), we can know which pins each motor is using.

From [this video](https://www.youtube.com/watch?v=fHAO7SW-SZI), we learned how to control a stepper motor with different speeds and directions.
