
function send_port(event) {
    const data = JSON.stringify({ serialPort: event.target.value })
    fetch('/serial-port', {
        headers: {
            'Content-Type': 'application/json',
        },
        method: 'POST',
        body: data,
    })
        .then((data) => {
            if (!data.ok) {
                // this will happen if there is a 404 or other error from the API
                throw data;
            }

            const result = document.querySelector('.result');
            result.setAttribute("style", "color:green;")
            result.textContent = 'Ready to output ✅'
            return
        })
        // catch network issues
        .catch(async (error) => {
            const data = await error.json()

            const result = document.querySelector('.result');
            result.setAttribute("style", "color:red;")
            result.textContent = `🆘 ${data.message} 🆘`
        });
}

function move_left() {
    fetch('/move-left')
        .then((data) => {
            if (!data.ok) {
                // this will happen if there is a 404 or other error from the API
                throw data;
            }

        })
        // catch network issues
        .catch(async (error) => {
            const data = await error.json()

        });
}

function move_right() {
    fetch('/move-right')
        .then((data) => {
            if (!data.ok) {
                // this will happen if there is a 404 or other error from the API
                throw data;
            }

        })
        // catch network issues
        .catch(async (error) => {
            const data = await error.json()

        });
}


function move_stop() {
    fetch('/move-stop')
        .then((data) => {
            if (!data.ok) {
                // this will happen if there is a 404 or other error from the API
                throw data;
            }

        })
        // catch network issues
        .catch(async (error) => {
            const data = await error.json()

        });
}

function scoop_open() {
    fetch('/scoop-open')
        .then((data) => {
            if (!data.ok) {
                // this will happen if there is a 404 or other error from the API
                throw data;
            }

        })
        // catch network issues
        .catch(async (error) => {
            const data = await error.json()

        });
}

function scoop_close() {
    fetch('/scoop-close')
        .then((data) => {
            if (!data.ok) {
                // this will happen if there is a 404 or other error from the API
                throw data;
            }

        })
        // catch network issues
        .catch(async (error) => {
            const data = await error.json()

        });
}

function auto_start() {
    fetch('/auto-start')
        .then((data) => {
            if (!data.ok) {
                // this will happen if there is a 404 or other error from the API
                throw data;
            }

        })
        // catch network issues
        .catch(async (error) => {
            const data = await error.json()

        });
}

function auto_reset() {
    fetch('/auto-reset')
        .then((data) => {
            if (!data.ok) {
                // this will happen if there is a 404 or other error from the API
                throw data;
            }

        })
        // catch network issues
        .catch(async (error) => {
            const data = await error.json()

        });
}

const selectElement = document.querySelector('.serial-port');
selectElement.addEventListener('change', send_port)

const moveLeftButtonElement = document.querySelector('.buttons .left');
moveLeftButtonElement.onclick = move_left

const moveRightButtonElement = document.querySelector('.buttons .right');
moveRightButtonElement.onclick = move_right

const moveStopButtonElement = document.querySelector('.buttons .stop');
moveStopButtonElement.onclick = move_stop


const scoopOpenButtonElement = document.querySelector('.buttons .scoop1');
scoopOpenButtonElement.onclick = scoop_open

const scoopCloseButtonElement = document.querySelector('.buttons .scoop2');
scoopCloseButtonElement.onclick = scoop_close

const autoStartButtonElement = document.querySelector('.pannel .auto-start');
autoStartButtonElement.onclick = auto_start

const autoResetButtonElement = document.querySelector('.pannel .reset');
autoResetButtonElement.onclick = auto_reset

const autoStopButtonElement = document.querySelector('.pannel .auto-stop');
autoStopButtonElement.onclick = move_stop