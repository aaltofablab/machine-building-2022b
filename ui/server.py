import serial
import serial.tools.list_ports
import time
from flask import Flask, render_template, request, jsonify

app = Flask(__name__)

serial_port = None

@app.route('/')
def index():
    ports = sorted(serial.tools.list_ports.comports())
    return render_template('index.html', serial_ports=ports)

@app.route('/serial-port', methods=['POST'])
def change_port():
    global serial_port
    try:
        serial_port = serial.Serial(request.json['serialPort'], 115200)
    except serial.SerialException as e:
        return jsonify({'message': str(e)}), 500

    serial_port.write('\r\n\r\n'.encode())
    time.sleep(2)
    serial_port.flushInput()

    return "OK"

@app.route('/move-left', methods=['GET'])
def move_left():
    serial_port.write('l'.encode())

    return "OK"

@app.route('/move-right', methods=['GET'])
def move_right():

    serial_port.write('r'.encode())

    return "OK"

@app.route('/move-stop', methods=['GET'])
def move_stop():
    serial_port.write('s'.encode())

    return "OK"


@app.route('/scoop-open', methods=['GET'])
def scoop_open():
    serial_port.write('o'.encode())

    return "OK"


@app.route('/scoop-close', methods=['GET'])
def scoop_close():
    serial_port.write('c'.encode())

    return "OK"


@app.route('/auto-start', methods=['GET'])
def auto_start():
    serial_port.write('a'.encode())

    return "OK"


@app.route('/auto-reset', methods=['GET'])
def auto_reset():
    serial_port.write('v'.encode())

    return "OK"