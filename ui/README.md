# ScoopUI

Web UI for our scoop

## Requirements

Python>=3.7

## Getting started

First create a virtual environment:

    python3 -m venv venv

Then activate it:

    . venv/bin/activate

After this install dependencies with

    pip install -r requirements.txt

After this you can run the server with:

    FLASK_APP=server.py FLASK_ENV=development flask run

The web ui should now be accessible at `http://127.0.0.1:5000`

## Cat Litter Controller

![Control UI](/assets/images/ui.png)

We design a simple interface to test and run the machine. It sends the command (a chracter) to Arduino by serial port and the motors will move according to the command.

#### Manual Mode

This mode is for testing the machine, to see if the basic movement works:

- MOVE LEFT: The scoop will keep going forward.
- MOVE RIGHT: The scoop will keep going backward.
- STOP: Stop all the motors.
- OPEN SCOOP: The scoop will rotate and lift up. How much the rotating angle can be set in Arduino code.
- CLOSE SCOOP: The scoop will rotate and go down. How much the rotating angle can be set in Arduino code.

#### Auto Mode

This mode is for testing the final function of the machine:

- START: The scoop will move from one side to another side and pull the poops together. When it arrive another side, the scoop will lift up and move back to the starting point. This function is the main function of this machine, which can clean the cat's poops automatically.

- STOP (haven't finished yet): This function acts like a emergency stop button. It can pause all the operation of the machine. This fucntion also can be developed to connect to a sensor on the machine, so a moving scoop will stop when a cat or something is close it.

- RESET (haven't finished yet): This function can move the scoop back to the starting point no matter where is the scoop now.
