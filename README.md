# Machine Building 2022

This readme-file is for the Machine building assignment of Fabacademy 2022.

This particular git repository contains the instructions and workfiles for building motor driven a kitty litter scooper, with a web-control interface, using materials and tools available at fablabs.

![heroshot](/assets/images/heroshot.jpg)

Click the image below to see a introduction video!

[![video](/assets/images/demovid.jpg)](/assets/videos/demo.mp4)

## Documentation

The documentation is organized by folders,

- Assembly instructions (contains a guide for assembling the machine)
- Circuit program (Contains the details of the elecronics and the arduino-code used to control the electronics)
- Design files (Contains vector drawings for the laser cut parts and 3D models for the 3D printed parts)
- Assets (Contains images and videos of the ready machine)
- Ui (Contains the web-based UI, and contains its own readme for compatability and software requirements)

## Authors

<!---
Each author needs to add a link to their fabpage containting the relevant personal documentation
-->
- Yuhan Tseng
- [Anna Li](https://yeshouanna.gitlab.io/aalto-digital-fabrication-2022/assignments/weekly/assignment13/)
- [Fiia Kitanoja](https://digital-fabrication.fiikuna.fi/)
- [Samuli Kärki](https://ambivalent.world/assignments/machine-building/ )
- [Karl Mihhels](https://karlpalo.gitlab.io/fablab2022/weekly/week13)

## License

Decide on a license you want to use for your machine building project.
